import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  AsyncStorage
} from 'react-native';
import {
  LectureCard,
} from '../presentation';

import CONFIG from '../../config';

export default class LectureList extends Component {

  componentWillMount() {
    this.lectures = CONFIG.lectureList;
  }

  _renderPost({ item }){
    return <LectureCard item={item} mine={false}/>;
}

  _returnKey(item){
    return item.title;
  }

  render() {
    return(
      <View style={{flex: 1, paddingBottom: 60}}>
        <FlatList
          data={this.lectures}
          keyExtractor={this._returnKey}
          renderItem={this._renderPost.bind(this)}
        >
        </FlatList>
      </View>
    )
  }

}
