import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {
  EventCard,
  NoItems
} from '../presentation';
import firebase from 'react-native-firebase';
import CONFIG from '../../config';

export default class MyEventList extends Component {

  constructor() {
    super();

    this.state = {
      myEvents: null
    }
  }

  componentWillMount() {
    this.events = CONFIG.eventList;

    eventsName = [];

    const ref = firebase.database().ref("/events");

    ref.on('value',(snap) => {
      AsyncStorage.setItem("events", JSON.stringify(snap._value));
      //console.log("updated events time in AsyncStorage");
    })

    firebase.database().ref("/eventsName").on('value',(snap) => {
      AsyncStorage.setItem("eventsName", JSON.stringify(snap._value));
      //console.log("updated events name in AsyncStorage");
    })

    this.updateMyEvents();
    intervalId = setInterval(this.updateMyEvents.bind(this), 1000);

    this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  updateMyEvents()  {
    this.events = CONFIG.eventList;
    let myEvents = [];

    AsyncStorage.getItem("myEvents")
      .then((values) => {
        values = JSON.parse(values);
        if(typeof(values) == "object"){
          for(let i=0; i<values.length; i++) {
            myEvents.push(this.events[values[i]]);
          }
          this.setState({
            myEvents: myEvents
          })
        }
      })
      .catch((e) =>{
        //AsyncStorage.setItem("events", JSON.stringify(snap._value));
        //console.log("myEvents AsyncStorage not present");
      })
  }

  _renderPost({ item }){
    return <EventCard item={item} mine={true}/>;
  }

  _returnKey(item){
    return item.title;
  }

  render() {
    return(
      <View style={{flex: 1, paddingBottom: 60}}>
      {
        (this.state.myEvents && this.state.myEvents.length!= 0)
        ?
          <FlatList
            data={this.state.myEvents}
            keyExtractor={this._returnKey}
            renderItem={this._renderPost.bind(this)}
          >
          </FlatList>
        :
          <NoItems />
      }


      </View>
    )
  }

}
