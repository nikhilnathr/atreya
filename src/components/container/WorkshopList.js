import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  AsyncStorage
} from 'react-native';
import {
  WorkshopCard,
} from '../presentation';

import CONFIG from '../../config';

export default class WorkshopList extends Component {

  componentWillMount() {
    this.workshops = CONFIG.workshopList;
  }

  _renderPost({ item }){
    return <WorkshopCard item={item} />;
  }

  _returnKey(item){
    return item.title;
  }

  render() {
    return(
      <View style={{flex: 1, paddingBottom: 60}}>
        <FlatList
          data={this.workshops}
          keyExtractor={this._returnKey}
          renderItem={this._renderPost.bind(this)}
        >
        </FlatList>
      </View>
    )
  }

}
