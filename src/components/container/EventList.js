import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  AsyncStorage
} from 'react-native';
import {
  EventCard,
} from '../presentation';

import CONFIG from '../../config';

export default class EventList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      eventsName: [],
    }
  }

  componentWillMount() {
    this.events = CONFIG.eventList;
  }

  _renderPost({ item }){
    return <EventCard item={item} mine={false} />;
  }

  _returnKey(item){
    return item.id+"";
  }

  render() {
    return(
      <View style={{flex: 1, paddingBottom: 60}}>
        <FlatList
          data={this.events}
          keyExtractor={this._returnKey}
          renderItem={this._renderPost.bind(this)}
        >
        </FlatList>
      </View>
    )
  }

}
