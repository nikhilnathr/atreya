import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {
  WorkshopCard,
  NoItems
} from '../presentation';
import firebase from 'react-native-firebase';
import CONFIG from '../../config';
import { Notification } from '../services';

export default class MyWorkshopList extends Component {

  constructor() {
    super();

    this.state = {
      myWorkshops: null
    }
  }

  componentWillMount() {

    this.workshops = CONFIG.workshopList;

    const ref = firebase.database().ref("/workshops");

    ref.on('value',(snap) => {
      AsyncStorage.setItem("workshops", JSON.stringify(snap._value));
      //console.log("updated workshops time in AsyncStorage");
    })

    this.updateMyWorkshops();
    intervalId = setInterval(this.updateMyWorkshops.bind(this), 1000);

    this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  updateMyWorkshops()  {
    this.workshops = CONFIG.workshopList;
    let myWorkshops = [];

    AsyncStorage.getItem("myWorkshops")
      .then((values) => {
        values = JSON.parse(values);
        if(typeof(values) == "object"){
          for(let i=0; i<values.length; i++) {
            myWorkshops.push(this.workshops[values[i]]);
          }
          this.setState({
            myWorkshops: myWorkshops
          })
        }
      })
      .catch((e) =>{
        //AsyncStorage.setItem("workshops", JSON.stringify(snap._value));
        //console.log("myWorkshops AsyncStorage not present");
      })
  }

  _renderPost({ item }){
    return <WorkshopCard item={item} mine={true}/>;
  }

  _returnKey(item){
    return item.title;
  }

  render() {
    return(
      <View style={{flex: 1, paddingBottom: 60}}>
      {
        (this.state.myWorkshops && this.state.myWorkshops.length!= 0)
        ?
          <FlatList
            data={this.state.myWorkshops}
            keyExtractor={this._returnKey}
            renderItem={this._renderPost.bind(this)}
          >
          </FlatList>
        :
          <NoItems />

      }


      </View>
    )
  }

}
