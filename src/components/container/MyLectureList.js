import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {
  NoItems,
  LectureCard,
} from '../presentation';
import firebase from 'react-native-firebase';
import CONFIG from '../../config';

export default class MyLectureList extends Component {

  constructor() {
    super();

    this.state = {
      myLectures: null
    }
  }

  componentWillMount() {
    this.lectures = CONFIG.lectureList;

    const ref = firebase.database().ref("/lectures");


    ref.on('value',(snap) => {
      AsyncStorage.setItem("lectures", JSON.stringify(snap._value));
      //console.log("updated lectures time in AsyncStorage");
    })
    
    this.updateMyLectures();
    intervalId = setInterval(this.updateMyLectures.bind(this), 1000);

    this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  updateMyLectures()  {
    this.lectures = CONFIG.lectureList;
    let myLectures = [];

    AsyncStorage.getItem("myLectures")
      .then((values) => {
        values = JSON.parse(values);
        if(typeof(values) == "object"){
          for(let i=0; i<values.length; i++) {
            myLectures.push(this.lectures[values[i]]);
          }
          this.setState({
            myLectures: myLectures
          })
        }
      })
      .catch((e) =>{
        //AsyncStorage.setItem("lectures", JSON.stringify(snap._value));
        //console.log("myLectures AsyncStorage not present");
      })
  }

  _renderPost({ item }){
    return <LectureCard item={item} mine={true}/>;
  }

  _returnKey(item){
    return item.title;
  }

  render() {
    return(
      <View style={{flex: 1, paddingBottom: 60}}>
      {
        (this.state.myLectures && this.state.myLectures.length!= 0)
        ?
          <FlatList
            data={this.state.myLectures}
            keyExtractor={this._returnKey}
            renderItem={this._renderPost.bind(this)}
          >
          </FlatList>
        :
          <NoItems />

      }


      </View>
    )
  }

}
