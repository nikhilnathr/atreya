import Workshops from  './Workshops.js'
import Lectures from  './Lectures.js'
import Events from  './Events.js'
import MyWorkshops from  './MyWorkshops.js'
import MyLectures from  './MyLectures.js'
import MyEvents from  './MyEvents.js'
import Noti from  './Noti.js'
import Splash from  './Splash.js'
import Location from  './Location.js'


export  {
  Workshops,
  Lectures,
  Events,
  MyWorkshops,
  MyLectures,
  MyEvents,
  Splash,
  Noti,
  Location
}
