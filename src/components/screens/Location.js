import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableNativeFeedback,
  Alert,
  PermissionsAndroid
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

export default class Location extends Component {

  constructor() {
    super();

    this.state = {
      region: {
        longitudeDelta: 0.0093352490067481995,
        latitudeDelta: 0.009910838046912062,
        longitude: 75.935936,
        latitude: 11.322338
      },
      markers: [],
      locationWorkaround: 5,
      locationMarkers: [
      	{
      		name: "Basketball Court",
      		location:{
      			longitude:11.321425,
      			latitude: 75.933399
      		}
      	},
      	{
      		name: "ELHC",
      		location:{
      			longitude:11.322525,
      			latitude:75.933837
      		}
      	},
      	{
      		name: "Audi",
      		location:{
      			longitude:11.322506,
      			latitude: 75.935845
      		}
      	},
      	{
      		name: "Electrical Lab",
      		location:{
      			longitude:11.322872,
      			latitude:75.935030
      		}
      	},
      	{
      		name: "SSL",
      		location:{
      			longitude:11.322827,
      			latitude:75.934206
      		}
      	},
      	{
      		name: "Electronics Lab",
      		location:{
      			longitude:11.322872,
      			latitude:75.935030
      		}
      	},
      	{
      		name: "Lecture Halls",
      		location:{
      			longitude:11.320928,
      			latitude:75.933930
      		}
      	}
      ],
      permissionGranted: false,
      markersMined: [{
        name: "Food Stall",
        description: "",
        location:{
          longitude: 75.934092,
          latitude: 11.321478
        }
      },{
        name: "ECLC",
        description: "Workshops",
        location:{
          latitude:11.322662,
          longitude: 75.937949
        }
      }]
    }
  }

  componentDidMount() {
    this.askPermission();
    this.getLocation();
    setTimeout(() => {
      this.setState({
        locationWorkaround: 0
      })
    }, 200)
  }

  componentDidUpdate () {
    this.updateCallout()
  }

  updateCallout () {
    if(this.marker) {
      this.marker.showCallout()
    }
  }

  componentWillUnmount() {
    if(this.state.permissionGranted){
      navigator.geolocation.clearWatch(this.watchID);
    }
  }

  buttonPressed(locationMarker){
    region = {
      longitudeDelta: 0.0053352490067481995,
      latitudeDelta: 0.008910838046912062,
      longitude: locationMarker.location.latitude,
      latitude: locationMarker.location.longitude
    }
    this.map.animateToRegion(region, 500)
    this.setState({
      markers: [{
        name: locationMarker.name,
        location: {
          longitude: locationMarker.location.latitude,
          latitude: locationMarker.location.longitude
        }
      }]
    })
  }

  async askPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Enable Location?',
          message:
            'Allow Atreya to access ' +
            'this device\'s location',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //Permission Granted
        this.setState({
          permissionGranted: true
        })
      } else {
        //console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async getLocation() {
    if(this.state.permissionGranted){
      navigator.geolocation.getCurrentPosition(position => {},
        (error) => {
          Alert.alert("Location Unavailable","Please turn on GPS to know your location")
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
      this.watchID = navigator.geolocation.watchPosition(
        position => {}
      );
    }

  }

  render() {
    return (
      <View style={{flex: 1}}>
        <MapView
          style={{
            flex: 8,
            marginTop: this.state.locationWorkaround
          }}
          ref={map => { this.map = map }}
          showsUserLocation={true}
          followsUserLocation={true}
          showsMyLocationButton={true}
          zoomEnabled={true}
          provider={PROVIDER_GOOGLE}
          customMapStyle={myMapStyles}
          initialRegion={this.state.region}
          showsIndoors={true}
          showScale={true}
        >
        {
            this.state.markersMined.map(markerMine => {
              return (
                <Marker key={JSON.stringify(markerMine.location)}
                  coordinate={markerMine.location}
                >
                  <View style={{padding: 5, backgroundColor: '#512828', borderRadius: 5, alignItems: 'center'}}>
                    <Text style={{color: "#FFFFFF", fontWeight: 'bold'}}>{markerMine.name}</Text>
                    {
                      (markerMine.description != "") &&
                      <Text style={{color: "#CCCCCC"}}>{markerMine.description}</Text>
                    }
                  </View>
                </Marker>
              )
          })
        }
        {
            this.state.markers.map(marker => {
              return (
                <Marker key={JSON.stringify(marker.location)}
                  coordinate={marker.location}
                  title={marker.name}
                  ref={_marker => {this.marker = _marker}}
                >
                </Marker>
              )
          })
        }
        </MapView>
        <View style={{height: 60, position: 'absolute', bottom: 0}}>
          <ScrollView
            style={{
              flex: 1,
              backgroundColor: "transparent",
            }}
            horizontal={true}
          >
            {
              this.state.locationMarkers.map(locationMarker =>{
                return(
                  <TouchableNativeFeedback
                    key={locationMarker.name}
                    onPress={this.buttonPressed.bind(this, locationMarker)}
                    background={TouchableNativeFeedback.Ripple('#000000')}>
                    <View style={styles.button}>
                      <Text style={styles.textColor}>{locationMarker.name}</Text>
                    </View>
                  </TouchableNativeFeedback>
                )
              })
            }


          </ScrollView>
        </View>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#FFFFFF",
    marginHorizontal: 10,
    minWidth: 140,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },

  textColor: {
    color: "#000000",
    fontSize: 15,
    fontWeight: 'bold'
  }
})

const myMapStyles = [
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "saturation": 36
      },
      {
        "lightness": 40
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 16
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 20
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 17
      },
      {
        "weight": 1.2
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "stylers": [
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "stylers": [
      {
        "saturation": "-100"
      },
      {
        "lightness": "30"
      },
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.province",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "stylers": [
      {
        "lightness": "74"
      },
      {
        "gamma": "0.00"
      },
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 20
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "stylers": [
      {
        "lightness": "3"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 21
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 18
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 17
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 29
      },
      {
        "weight": 0.2
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 16
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 19
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 17
      }
    ]
  }
]
