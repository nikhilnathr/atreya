import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ScrollView
} from 'react-native';

import  {
  MyWorkshopList
} from '../container';

import {
  BottomGradient
} from '../presentation';

export default class MyWorkshops extends Component {
  render() {
    return(
      <View style={styles.container}>
        <ScrollView style={{flex: 1, paddingTop: 20}}>
          <MyWorkshopList />
        </ScrollView>
        <BottomGradient />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000",
  },
  card: {
    width: "100%",
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    width: "20%",
    alignItems: 'center'

  },
  textContainer: {

  },
  text: {
    color: "#FFFFFF",
    fontSize: 20
  }
})
