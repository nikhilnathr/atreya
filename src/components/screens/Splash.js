import React, { Component } from 'react';
import {
  Text,
  View,
  Animated,
  Easing,
  Dimensions,
  StyleSheet,
  Image
} from 'react-native';

import CONFIG from '../../config';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

export default class Splash extends Component {

  constructor(props) {
    super(props);

    this.state = {
      closingAnim: new Animated.Value(HEIGHT + 200),
      fadeInAnim: new Animated.Value(0),
    }
  }


  componentDidMount() {

      Animated.timing(
        this.state.closingAnim,
        {
          toValue: 0,
          duration: 400,
          Easing: Easing.ease
        }
      ).start(() => {
        Animated.timing(
          this.state.fadeInAnim,
          {
            toValue: 1,
            duration: 800,
            Easing: Easing.bounce
          }
        ).start(() => {
          setTimeout(() => {
            this.props.navigation.navigate("Main");
          }, 200)
        })
      });

  }

  render() {

    return(
      <View style={styles.container}>
        <Animated.View style={{position: 'absolute', backgroundColor: "#FFFFFF", width: this.state.closingAnim, height: this.state.closingAnim, borderRadius: this.state.closingAnim}}></Animated.View>
        <Animated.Image source={CONFIG.splash_logo} style={{position: 'absolute', height: 200, width: 200, opacity: this.state.fadeInAnim}}/>
      </View>
    )

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000",
    justifyContent: 'center',
    alignItems: 'center'
  },
})
