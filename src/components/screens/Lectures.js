import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ScrollView
} from 'react-native';

import  {
  LectureList
} from '../container';

import {
  BottomGradient
} from '../presentation';

export default class Lectures extends Component {
  render() {
    return(
      <View style={styles.container}>
        <ScrollView style={{flex: 1, paddingTop: 20}}>
          <LectureList />
        </ScrollView>
        <BottomGradient />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000",
  },
})
