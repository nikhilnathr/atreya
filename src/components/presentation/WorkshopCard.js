import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  Image,
  TouchableNativeFeedback,
  ToastAndroid,
  ActivityIndicator
} from 'react-native';

import CONFIG from '../../config';

export default class WorkshopCard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      workshopTime: this.props.item.time,
      time: null,
      completed: false,
      fetched: false,
      presentInMine: false,
    }
  }

  componentDidMount() {
    this.rippleColor = "#000000";
    if(!this.props.mine) {
      this.rippleColor = "#CCCCCC";
    }
    AsyncStorage.getItem('myWorkshops')
      .then((values) => {
        if(values != null && values.indexOf(this.props.item.id) > -1){
          this.setState({
            presentInMine: true
          })
        }
      })
    var intervalId = setInterval(this.startCounter, 1000);
    //AsyncStorage.setItem("myWorkshops", "");

    this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  startCounter = () => {
    AsyncStorage.getItem('workshops')
    .then((value)  => {
      let asyncTime = JSON.parse(value)[this.props.item.id].time;
      this.setState({
        workshopTime: asyncTime,
        fetched: true
      })
    })
    .catch ((error) => {
      this.setState({
        fetched: true
      })
    })

    if(this.state.fetched) {
      let dummy = new Date(),
      curTime = Math.floor((dummy.valueOf())/ 1000) - (dummy.getTimezoneOffset()*60);
      if(curTime > this.state.workshopTime) {
        this.setState({
          completed: true
        })
      }

      timeDiff = eval(this.state.workshopTime - curTime)*1000,
      date = new Date(timeDiff),
      hours = date.getUTCHours() + ((date.getUTCDate() - 1)*24),
      minutes = "0" + date.getUTCMinutes(),
      seconds = "0" + date.getUTCSeconds();

      this.setState({
        time: (hours + 'h ' + minutes.substr(-2) + 'm ' + seconds.substr(-2) +'s')
      })
    }
  }

  _onPressButton() {
    if(this.props.mine) {
      return;
    }
    if(this.state.presentInMine) {
      AsyncStorage.getItem("myWorkshops")
        .then((value) => {
          var prevValue = [];
          prevValue = JSON.parse(value);

          prevValue.splice(prevValue.indexOf(this.props.item.id), 1);

          AsyncStorage.setItem("myWorkshops", JSON.stringify(prevValue));
          this.setState({
            presentInMine: false
          })
          ToastAndroid.show(this.props.item.title  +  " removed", ToastAndroid.SHORT);


        })
        .catch((e) => {

        })
    } else {
      AsyncStorage.getItem("myWorkshops")
        .then((value) => {
          var prevValue = [];
          prevValue = JSON.parse(value);


          prevValue.push(this.props.item.id)

          AsyncStorage.setItem("myWorkshops", JSON.stringify(prevValue));
          this.setState({
            presentInMine: true
          })
          ToastAndroid.show(this.props.item.title  +  " added to your items", ToastAndroid.SHORT);


        })
        .catch((e) => {
          var prevValue = [];
          prevValue.push(this.props.item.id)

          AsyncStorage.setItem("myWorkshops", JSON.stringify(prevValue));
          this.setState({
            presentInMine: true
          })
          ToastAndroid.show(this.props.item.title  +  " added to your items", ToastAndroid.SHORT);
        })

    }



  }

  render() {
    return(
      <TouchableNativeFeedback
        onPress={this._onPressButton.bind(this)}
        background={TouchableNativeFeedback.Ripple(this.rippleColor)}>
        <View style={[styles.card, !this.props.mine && this.state.presentInMine && {backgroundColor: CONFIG.elementAddedToMineColor}, this.props.mine && !this.state.presentInMine && {display: "none"}]}>
          <View style={styles.heading}>
            <View style={styles.iconContainer}>
              <Image source={this.props.item.icon} style={styles.icon}/>
            </View>
            <View style={styles.textContainer}>
              <View style={{paddingVertical: 3}}>
                <Text style={styles.headingText}>
                  {this.props.item.title}
                </Text>
              </View>
              <View style={{marginVertical: 5}}>
                {
                  (this.state.completed)
                  ?
                    <Text style={styles.startsinText}>
                      Started
                    </Text>
                  :
                    (this.state.time)
                    ?
                      <Text style={styles.startsinText}>
                        Starts in: {this.state.time}
                      </Text>
                    :
                      <View style={{width: 40}}>
                        <ActivityIndicator size="small" color="#FFFFFF" />
                      </View>
                }

              </View>
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>
    );
  }

}

const styles = StyleSheet.create({

  card: {
    paddingHorizontal: 20,
  },

  heading: {
    flexDirection: 'row',
    paddingVertical: 40,
    alignItems: 'center'
  },

  icon: {
    height: 55,
    width: 55
  },

  iconContainer: {
    width: "25%",
    alignItems: 'center',
  },

  textContainer: {
    width: "75%",
  },

  headingText: {
    fontSize: 18,
    color: "#FFFFFF"
  },

  startsinText: {
    fontSize: 16,
    color: "#FFFFFF"
  }

})
