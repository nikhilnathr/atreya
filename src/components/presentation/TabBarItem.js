import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class TabBarItem extends Component {
  render() {
    return(
      <View style={{flex: 1, flexDirection: 'row', height: 80, alignItems: 'center'}}>
        <Icon name={"md-"+this.props.icon} style={{paddingHorizontal: 10}} size={24} color="#ffffff"/>
        <Text style={{color:"#FFFFFF"}}>{this.props.name}</Text>
      </View>
    )
  }
}
