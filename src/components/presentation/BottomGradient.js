import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';

export default class BottomGradient extends Component {
  render() {
    return (
      <LinearGradient
        colors={["transparent", "#000000"]}
        style={{position: 'absolute', bottom: 0, width: "100%", height: 40}}
      >
      </LinearGradient>
    );
  }
}
