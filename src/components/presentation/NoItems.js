import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native'

export default class NoItems extends Component {

  render() {
    return(
      <View style={{flex: 1, alignItems: 'center', paddingTop: 40}}>
        <Text style={{color: "#888888", fontSize: 16}}>Start by adding items from the "All" section</Text>
      </View>
    )
  }
}
