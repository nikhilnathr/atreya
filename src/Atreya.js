import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import { createStackNavigator, createSwitchNavigator, createMaterialTopTabNavigator, createBottomTabNavigator, createAppContainer } from "react-navigation";

import {
  Workshops,
  Lectures,
  Events,
  MyWorkshops,
  MyLectures,
  MyEvents,
  Splash,
  Location
} from './components/screens';
import {
  TabBarItem
} from './components/presentation';
import Icon from 'react-native-vector-icons/Ionicons';

const AllTopNavigator = createMaterialTopTabNavigator({
  Workshops: {
    screen: Workshops,
    navigationOptions: {
      tabBarLabel: <TabBarItem name="Workshops" icon="build"/>,
    }
  },
  Lectures: {
    screen: Lectures,
    navigationOptions: {
      tabBarLabel: (<TabBarItem name="Lectures" icon="book"/>),
    }
  },
  Events: {
    screen: Events,
    navigationOptions: {
      tabBarLabel: (<TabBarItem name="Events" icon="football"/>),
    }
  },
},
{
  tabBarOptions: {
    //showLabel: false,
    activeTintColor: '#FFFFFF',
    inactiveTintColor: '#444444',
    pressColor: "#111111",
    labelStyle: {
      //fontSize: 15,
    },
    tabStyle: {
      alignItems: 'center',
    },
    indicatorStyle: {
      backgroundColor: '#FFFFFF',
      width: "10%",
      marginLeft: "10%"
    },
    style:{
      height: 80,
      width: "100%",
      backgroundColor: "#000000",
    }
  },
});

const MineTopNavigator = createMaterialTopTabNavigator({
  MyWorkshops: {
    screen: MyWorkshops,
    navigationOptions: {
      tabBarLabel: <TabBarItem name="Workshops" icon="build"/>,
    }
  },
  MyLectures: {
    screen: MyLectures,
    navigationOptions: {
      tabBarLabel: (<TabBarItem name="Lectures" icon="book"/>),
    }
  },
  MyEvents: {
    screen: MyEvents,
    navigationOptions: {
      tabBarLabel: (<TabBarItem name="Events" icon="football"/>),
    }
  },
},
{
  tabBarOptions: {
    //showLabel: false,
    activeTintColor: '#FFFFFF',
    inactiveTintColor: '#444444',
    pressColor: "#111111",
    labelStyle: {
      //fontSize: 15,
    },
    tabStyle: {
      alignItems: 'center',
    },
    indicatorStyle: {
      backgroundColor: '#FFFFFF',
      width: "10%",
      marginLeft: "10%"
    },
    style:{
      height: 80,
      width: "100%",
      backgroundColor: "#000000",
    }
  },
});

const MainTabNavigator = createBottomTabNavigator(
  {
    Mine: {
      screen: MineTopNavigator,
      navigationOptions: {
        tabBarLabel: "My Items",
      }
    },
    All: {
      screen: AllTopNavigator,
      navigationOptions: {
        tabBarLabel: "All Items",
      }
    },
    Location: {
      screen: Location,
      navigationOptions: {
        tabBarLabel: "Locations",
      }
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Mine') {
          //iconName = `md-person${focused ? '' : '-outline'}`;
          iconName = "md-person"
        } else if (routeName === 'All') {
          iconName = "md-people";
        } else if (routeName === 'Location') {
          iconName = "md-pin";
        }

        // You can return any component that you like here!
        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#FFFFFF',
      inactiveTintColor: '#444444',
      style: {
        backgroundColor: "#000000",
        height: 50
      }
    },
  }
);

const AppNavigator = createSwitchNavigator(
  {
    Splash: Splash,
    Main: MainTabNavigator
  },
  {
    initialRouteName: "Splash"
  }
);

export default createAppContainer(AppNavigator);
