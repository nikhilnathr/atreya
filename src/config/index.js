export default{
  elementAddedToMineColor: "#222222",
  splash_logo: require('../../assets/logo_splash.png'),
  lectureList: [
    {
      id: 0,
      title: "Challenging the IAS Dream",
      lecturer: "Dr. A U Prasad",
      icon: require('../../assets/l0.png'),
      time: 1549796400,
    },
    {
      id: 1,
      title: "The New Space Ecosystem In India & World",
      lecturer: "Suraj Jana",
      icon: require('../../assets/l1.png'),
      time: 1549796400
    },
    {
      id: 2,
      title: "Is There A Science Of Stories?",
      icon: require('../../assets/l2.png'),
      lecturer: "Anil Menon",
      time: 1549796400
    },
    {
      id: 3,
      title: "Gravitational Waves: A New Window To The Universe",
      icon: require('../../assets/l3.png'),
      lecturer: "Dr. Rahul Kashyap, ICTS",
      time: 1549796400
    },
    {
      id: 4,
      title: "Generative Adversarial Network (Gan) Applications And Theory",
      icon: require('../../assets/l4.png'),
      lecturer: "Dr. Deepak Mishra",
      time: 1549796400
    }

  ],
  eventList: [
   {
      id: 0,
      title: "JAM",
      icon: require('../../assets/e0.png'),
      time: 1549796400
   },
   {
      id: 1,
      title: "Beat the Talk",
      icon: require('../../assets/e1.png'),
      time: 1549677600
   },
   {
      id: 2,
      title: "Artha General Quiz",
      icon: require('../../assets/e2.png'),
      time: 1549677600
   },
   {
      id: 3,
      title: "Crime Scene Investigation",
      icon: require('../../assets/e3.png'),
      time: 1549702800
   },
   {
      id: 4,
      title: "Techilla - Treasure Hunt",
      icon: require('../../assets/e4.png'),
      time: 1549791000
   },
   {
      id: 5,
      title: "Admania",
      icon: require('../../assets/e5.png'),
      time: 1549796400
   },
   {
      id: 6,
      title: "Frames",
      icon: require('../../assets/e6.png'),
      time: 1549330380
   },
   {
      id: 7,
      title: "Techscribe",
      icon: require('../../assets/e7.png'),
      time: 1549677600
   },
   {
      id: 8,
      title: "DreamCodex",
      icon: require('../../assets/e8.png'),
      time: 1549702800
   },
   {
      id: 9,
      title: "Deux Hax",
      icon: require('../../assets/e9.png'),
      time: 1549791000
   },
   {
      id: 10
      ,title: "Boolean",
      icon: require('../../assets/e10.png'),
      time: 1549702800
   },
   {
      id: 11
      ,title: "Analog IQ",
      icon: require('../../assets/e11.png'),
      time: 1549791000
   },
   {
      id: 12
      ,title: "Junkyard Wars",
      icon: require('../../assets/e12.png'),
      time: 1549791000
   },
   {
      id: 13
      ,title: "Solidworks",
      icon: require('../../assets/e13.png'),
      time: 1549711800
   },
   {
      id: 14
      ,title: "Death Race",
      icon: require('../../assets/e14.png'),
      time: 1549677600
   },
   {
      id: 15
      ,title: "Bob the Builder",
      icon: require('../../assets/e15.png'),
      time: 1549702800
   },
   {
      id: 16
      ,title: "AutoCad",
      icon: require('../../assets/e16.png'),
      time: 1549330380
   }
],
  workshopList: [
  	{
  		id: 0,
  		title: "Industrial Automation",
      icon: require('../../assets/w0.png'),
  		time: 1549796400
  	},
  	{
  		id: 1,
  		title: "Internal Combustion Engine",
      icon: require('../../assets/w1.png'),
  		time: 1549796400
  	},
  	{
  		id: 2,
  		title: "Augmented Reality",
      icon: require('../../assets/w2.png'),
  		time: 1549796400
  	},
  	{
  		id: 3,
  		title: "Cloud Computing",
      icon: require('../../assets/w3.png'),
  		time: 1549796400
  	},
  	{
  		id: 4,
  		title: "Building Information Modelling",
      icon: require('../../assets/w4.png'),
  		time: 1549796400
  	},
  	{
  		id: 5,
  		title: "Neural Network Using Python",
      icon: require('../../assets/w5.png'),
  		time: 1549796400
  	},
  ],
}
