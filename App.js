import React, {Component} from 'react';
import {
  View,
  StatusBar,
  AsyncStorage
} from 'react-native';
import { Notification } from './src/components/services';
import appConfig from './app.json';
import firebase from 'react-native-firebase';
import CONFIG from './src/config';
import Atreya from './src/Atreya.js';

export default class App extends Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      senderId: appConfig.senderID
    };

    this.notif = new Notification(this.onRegister.bind(this), this.onNotif.bind(this));
    this.events = CONFIG.eventList;
    this.lectures = CONFIG.lectureList;
    this.workshops = CONFIG.workshopList;
  }


  componentWillMount() {
    this.notif.cancelAll();
    this.counter = 0;
    dummy = new Date()
    this.curTime = Math.floor((dummy.valueOf())/ 1000) - (dummy.getTimezoneOffset()*60);

    firebase.database().ref("/events").on('value',(snap) => {
      vals = snap._value;
      AsyncStorage.setItem("events", JSON.stringify(vals));


      let i = 0;
      vals.forEach((val) => {
        if(this.curTime < val.time) {
          this.notif.scheduleNotif("Event Notification", "The event " + this.events[i].title + " is about to start soon.", "Event", this.events[i].title, val.time, this.counter);
          ////console.log("Scheduled: "+this.events[i].title);
        } else {
          ////console.log("Done: "+this.events[i].title);
        }
        i++;
        this.counter++;
      })
      ////console.log("Event Notifications updated");
    })


    firebase.database().ref("/lectures").on('value',(snap) => {
      vals = snap._value;
      AsyncStorage.setItem("lectures", JSON.stringify(vals));


      let i = 0;
      vals.forEach((val) => {
        if(this.curTime < val.time) {
          this.notif.scheduleNotif("Lecture Notification", "The lecture " + this.lectures[i].title + " by "+ this.events[i].lecturer + " is about to start soon.", "Lecture", this.lectures[i].title + " by " + this.lectures[i].lecturer, val.time, this.counter);
        }
        i++;
        this.counter++;
      })

      //console.log("Lecture Notifications updated");
    })

    firebase.database().ref("/workshops").on('value',(snap) => {
      vals = snap._value;
      AsyncStorage.setItem("workshops", JSON.stringify(vals));


      let i = 0;
      vals.forEach((val) => {
        if(this.curTime < val.time) {
          this.notif.scheduleNotif("Workshop Notification", "The workshop " + this.workshops[i].title + " is about to start soon.", "Workshop", this.workshops[i].title, val.time, this.counter);
        }
        i++;
        this.counter++;
      })

      //console.log("Workshops Notifications updated");
    })
  }

  render() {
    return (
      <View style={{flex: 1}}>
      <StatusBar
        color="#000000"
        />
        <Atreya />
      </View>
    );
  }

  onRegister(token) {
    ////console.log(token);
    //Alert.alert("Registered !", JSON.stringify(token));
    ////console.log(token);
    //this.setState({ registerToken: token.token, gcmRegistered: true });
  }

  onNotif(notif) {
    ////console.log(notif);
    //Alert.alert(notif.title, notif.message);
  }

  handlePerm(perms) {
    //Alert.alert("Permissions", JSON.stringify(perms));
  }
}
